import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { AuthScreen } from './src/components';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './src/reducers';

var wilddog = require('wilddog');

class App extends Component {
  componentWillMount() {
    wilddog.initializeApp({
      authDomain: "wd5586131105yfaxtj.wilddog.com" // update to your app id
    });
  }

  render() {
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(thunk))}>
        <AuthScreen />
      </Provider>
    );
  }
}

export default App;
